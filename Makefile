# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/13 13:15:33 by ngrasset          #+#    #+#              #
#    Updated: 2018/06/13 13:15:34 by ngrasset         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

COMMON_SRC=src/common.c src/common_fs.c src/common_error.c src/common_input.c
CLIENT_SRC=src/client.c src/common.c src/client_requests.c src/client_commands.c $(COMMON_SRC)
SERVER_SRC=src/server.c src/builtin_ls.c src/server_commands.c $(COMMON_SRC)

OBJ_DIR=.tmp

CLIENT_OBJ=$(patsubst src/%.c, $(OBJ_DIR)/%.o, $(CLIENT_SRC))
SERVER_OBJ=$(patsubst src/%.c, $(OBJ_DIR)/%.o, $(SERVER_SRC))

CFLAGS=-Wall -Wextra -Werror -I ./libft/includes
CC=clang

DEPS=src/ft_p.h

LIBS=-l readline -L ./libft -l ft

all: client server

client: $(CLIENT_OBJ)
	make -C libft
	$(CC) $^ -o $@ $(CFLAGS) $(LIBS)

server: $(SERVER_OBJ)
	make -C libft
	$(CC) $^ -o $@ $(CFLAGS) $(LIBS)

$(OBJ_DIR)/%.o: src/%.c $(DEPS)
	@mkdir -p .tmp
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	make clean -C libft
	rm -Rf .tmp

fclean: clean
	make fclean -C libft
	rm -f server client
