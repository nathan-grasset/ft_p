/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_commands.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 13:15:43 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 13:15:45 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

void	execute_command(int cs, t_ft_p_request request)
{
	if (request.command == FT_P_LS)
		execute_ls(cs);
	if (request.command == FT_P_PWD)
		execute_pwd(cs);
	if (request.command == FT_P_CD)
		execute_cd(cs, request);
	if (request.command == FT_P_GET)
		execute_get(cs, request);
	if (request.command == FT_P_PUT)
		execute_put(cs, request);
}

void	execute_cd(int cs, t_ft_p_request request)
{
	if (!is_path_allowed(request.arg))
		return ((void)send_response_error(cs, FT_P_EOUTPATH));
	if (chdir(request.arg) < 0)
	{
		perror("chdir");
		send_response_error(cs, FT_P_EBADDIR);
	}
	else
		send_response_success(cs);
}

void	execute_pwd(int cs)
{
	char	buffer[PATH_MAX];

	if (getcwd(buffer, PATH_MAX) == NULL)
	{
		perror("getcwd");
		send_response_error(cs, FT_P_ESERVER_INTERNAL);
	}
	else
		send_response_body(cs, buffer, PATH_MAX);
}

void	execute_get(int cs, t_ft_p_request request)
{
	char	*file;
	t_u32	file_size;

	file = read_file_from_disk(request.arg, &file_size);
	if (!file)
		send_response_error(cs, get_error());
	else
		send_response_body(cs, file, file_size);
}

void	execute_put(int cs, t_ft_p_request request)
{
	char	*body;

	body = read_request_body(cs, request);
	if (!body)
		send_response_error(cs, FT_P_EBODY_CORRUPTED);
	if (write_file_to_disk(request.arg, body, request.content_size) < 0)
		send_response_error(cs, FT_P_ESERVER_INTERNAL);
	else
		send_response_success(cs);
	free(body);
}
