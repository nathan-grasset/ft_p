/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 12:32:25 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 12:32:26 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_P_H
# define FT_P_H

# include <libft.h>
# include <stdint.h>
# include <limits.h>
# include <stdlib.h>

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>

# include <sys/stat.h>
# include <sys/socket.h>
# include <netdb.h>
# include <netinet/in.h>
# include <arpa/inet.h>

# include <readline/readline.h>
# include <readline/history.h>
# include <string.h>

# define MAX_FILE_SIZE 12000000

typedef uint32_t		t_u32;
typedef int32_t			t_s32;

typedef enum			e_ft_p_command
{
	FT_P_LS,
	FT_P_PWD,
	FT_P_CD,
	FT_P_GET,
	FT_P_PUT,
}						t_ft_p_command;

typedef enum			e_ft_p_error
{
	FT_P_EOK = 0,
	FT_P_ENOTFOUND = -1,
	FT_P_ETOOBIG = -2,
	FT_P_EBADDIR = -3,
	FT_P_EBADFILE = -4,
	FT_P_EBODY_CORRUPTED = -5,
	FT_P_ESERVER_INTERNAL = -6,
	FT_P_EOUTPATH = -7,
}						t_ft_p_error;

typedef struct			s_ft_p_request
{
	t_ft_p_command		command;
	char				arg[PATH_MAX];
	t_u32				content_size;
}						t_ft_p_request;

typedef struct			s_ft_p_response
{
	t_ft_p_error		error;
	char				message[256];
	t_u32				content_size;
}						t_ft_p_response;

int						write_file_to_disk(char *filename, char *data,
							t_u32 size);
char					*read_file_from_disk(char *filename, t_u32 *size_out);
char					*get_file_name(char *path);
void					log_ft_p_error(t_ft_p_error err);

int						read_request(int socket, t_ft_p_request *r);
int						read_response(int socket, t_ft_p_response *r);
char					*read_response_body(int socket, t_ft_p_response r);
char					*read_request_body(int socket, t_ft_p_request r);
int						send_response_success(int socket);
int						send_response_body(int socket, char *body, t_u32 size);
int						send_response_error(int socket, t_ft_p_error err);
int						send_request_arg(int socket, t_ft_p_command command,
							char *arg);
int						send_requestb_put(int socket, char *arg, char *body,
							t_u32 size);
int						send_request(int socket, t_ft_p_command command);

int						execute_ls(int cs);

t_ft_p_error			get_error(void);
t_ft_p_error			set_error(t_ft_p_error err);

int						send_request_ls(int sock);
int						send_request_pwd(int sock);
int						send_request_cd(int sock, char *arg);
int						send_request_get(int sock, char *arg);
int						send_request_put(int sock, char *arg);

int						client_parse_command(int sock, char *command);

void					execute_cd(int cs, t_ft_p_request request);
void					execute_pwd(int cs);
void					execute_get(int cs, t_ft_p_request request);
void					execute_put(int cs, t_ft_p_request request);
void					execute_command(int cs, t_ft_p_request request);

int						is_path_allowed(char *path);

#endif
