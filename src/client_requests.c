/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_requests.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 12:54:31 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 12:54:32 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

int		send_request_ls(int sock)
{
	int				code;
	t_ft_p_response response;
	char			*body;

	if ((code = send_request(sock, FT_P_LS)) < 0)
		return (code);
	if ((code = read_response(sock, &response)) < 0)
		return (code);
	printf("%s\n", response.message);
	if (response.error != FT_P_EOK)
	{
		log_ft_p_error(response.error);
		return (0);
	}
	body = read_response_body(sock, response);
	if (!body)
		return (-1);
	printf("%s\n", body);
	free(body);
	return (0);
}

int		send_request_pwd(int sock)
{
	int				code;
	t_ft_p_response	response;
	char			*body;

	if ((code = send_request(sock, FT_P_PWD)) < 0)
		return (code);
	if ((code = read_response(sock, &response)) < 0)
		return (code);
	printf("%s\n", response.message);
	if (response.error != FT_P_EOK)
	{
		log_ft_p_error(response.error);
		return (0);
	}
	body = read_response_body(sock, response);
	if (!body)
		return (-1);
	printf("%s\n", body);
	free(body);
	return (0);
}

int		send_request_cd(int sock, char *arg)
{
	int				code;
	t_ft_p_response	response;

	if ((code = send_request_arg(sock, FT_P_CD, arg)) < 0)
		return (code);
	if ((code = read_response(sock, &response)) < 0)
		return (code);
	printf("%s\n", response.message);
	if (response.error != FT_P_EOK)
		log_ft_p_error(response.error);
	return (0);
}

int		send_request_get(int sock, char *arg)
{
	int				code;
	t_ft_p_response	response;
	char			*body;

	if ((code = send_request_arg(sock, FT_P_GET, arg)) < 0)
		return (code);
	if ((code = read_response(sock, &response)) < 0)
		return (code);
	printf("%s\n", response.message);
	if (response.error != FT_P_EOK)
	{
		log_ft_p_error(response.error);
		return (0);
	}
	body = read_response_body(sock, response);
	if (!body)
		return (-1);
	write_file_to_disk(arg, body, response.content_size);
	free(body);
	return (0);
}

int		send_request_put(int sock, char *arg)
{
	int				code;
	t_ft_p_response	response;
	char			*file;
	t_u32			file_size;
	char			*filename;

	filename = get_file_name(arg);
	if (!filename)
	{
		fprintf(stderr, "ft_p error: Invalid file : %s\n", arg);
		return (0);
	}
	file = read_file_from_disk(arg, &file_size);
	if (!file)
		return (0);
	code = send_requestb_put(sock, filename, file, file_size);
	free(file);
	if (code < 0)
		return (code);
	if ((code = read_response(sock, &response)) < 0)
		return (code);
	printf("%s\n", response.message);
	if (response.error != FT_P_EOK)
		log_ft_p_error(response.error);
	return (0);
}
