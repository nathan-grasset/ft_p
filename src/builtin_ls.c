/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_ls.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 11:46:32 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 11:46:33 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"
#include <unistd.h>
#include <stdlib.h>

static char		*parent_ls(int *fd, t_u32 *content_size)
{
	char	*res;
	t_u32	total;
	t_u32	ret;

	close(fd[1]);
	res = (char *)malloc(255);
	total = 0;
	while ((ret = read(fd[0], res + total, 255)) > 0)
	{
		total += ret;
		res = (char *)realloc(res, total + 255);
	}
	if (content_size)
		*content_size = total;
	return (res);
}

static char		*execute_bin_ls(t_u32 *content_size)
{
	int		fd[2];
	int		pid;

	if (pipe(fd) < 0)
		return (NULL);
	if ((pid = fork()) < 0)
		return (NULL);
	if (pid > 0)
		return (parent_ls(fd, content_size));
	close(fd[0]);
	if (dup2(fd[1], 1) < 0)
		exit(-1);
	execl("/bin/ls", "/bin/ls", NULL);
	exit(-1);
}

int				execute_ls(int cs)
{
	char	*body;
	t_u32	size;

	body = execute_bin_ls(&size);
	if (!body)
		return (send_response_error(cs, FT_P_ESERVER_INTERNAL));
	return (send_response_body(cs, body, size));
}
