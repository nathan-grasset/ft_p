/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_input.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 13:32:04 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 13:32:05 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

int		read_request(int socket, t_ft_p_request *r)
{
	int		ret;

	ret = read(socket, r, sizeof(t_ft_p_request));
	if (ret < 0)
		return (ret);
	if (ret != sizeof(t_ft_p_request))
	{
		fprintf(stderr, "Received corrupted request\n");
		return (-1);
	}
	return (ret);
}

int		read_response(int socket, t_ft_p_response *r)
{
	int		ret;

	ret = read(socket, r, sizeof(t_ft_p_response));
	if (ret < 0)
		return (ret);
	if (ret != sizeof(t_ft_p_response))
	{
		fprintf(stderr, "Received corrupted response\n");
		return (-1);
	}
	return (ret);
}

char	*read_response_body(int socket, t_ft_p_response r)
{
	int		ret;
	char	*body;

	body = (char *)malloc(r.content_size + 1);
	if (!body)
		return (NULL);
	if (r.content_size == 0)
		return (strdup(""));
	ret = recv(socket, body, r.content_size, MSG_WAITALL);
	if (ret < 0)
	{
		free(body);
		return (NULL);
	}
	if ((t_u32)ret != r.content_size)
	{
		fprintf(stderr, "Received corrupted body: %d bytes vs %u expected\n",
				ret, r.content_size);
		free(body);
		return (NULL);
	}
	body[ret] = '\0';
	return (body);
}

char	*read_request_body(int socket, t_ft_p_request r)
{
	int		ret;
	char	*body;

	body = (char *)malloc(r.content_size + 1);
	if (!body)
		return (NULL);
	if (r.content_size == 0)
		return (strdup(""));
	ret = recv(socket, body, r.content_size, MSG_WAITALL);
	if (ret < 0)
	{
		free(body);
		return (NULL);
	}
	if ((t_u32)ret != r.content_size)
	{
		fprintf(stderr, "Received corrupted body: %d bytes vs %u expected\n",
				ret, r.content_size);
		free(body);
		return (NULL);
	}
	body[ret] = '\0';
	return (body);
}
