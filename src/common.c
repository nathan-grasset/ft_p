/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 11:46:38 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 11:46:39 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

int		send_response_success(int socket)
{
	t_ft_p_response	r;

	r.error = FT_P_EOK;
	r.content_size = 0;
	memset(r.message, 0, 256);
	strncpy(r.message, "SUCCESS", 8);
	return (write(socket, &r, sizeof(t_ft_p_response)));
}

int		send_response_body(int socket, char *body, t_u32 size)
{
	t_ft_p_response	r;

	r.error = FT_P_EOK;
	r.content_size = size;
	memset(r.message, 0, 256);
	strncpy(r.message, "SUCCESS", 8);
	write(socket, &r, sizeof(t_ft_p_response));
	return (write(socket, body, size));
}

int		send_request(int socket, t_ft_p_command command)
{
	t_ft_p_request r;

	r.command = command;
	r.content_size = 0;
	memset(r.arg, 0, PATH_MAX);
	return (write(socket, &r, sizeof(t_ft_p_request)));
}

int		send_request_arg(int socket, t_ft_p_command command, char *arg)
{
	t_ft_p_request r;

	r.command = command;
	r.content_size = 0;
	strncpy(r.arg, arg, PATH_MAX);
	return (write(socket, &r, sizeof(t_ft_p_request)));
}

int		send_requestb_put(int socket, char *arg, char *body, t_u32 size)
{
	t_ft_p_request r;

	r.command = FT_P_PUT;
	r.content_size = size;
	strncpy(r.arg, arg, PATH_MAX);
	write(socket, &r, sizeof(t_ft_p_request));
	return (write(socket, body, size));
}
