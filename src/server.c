/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 11:46:43 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 11:46:44 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

static char g_base_path[PATH_MAX];

void	usage(char *name)
{
	fprintf(stderr, "Usage %s <port>\n", name);
	exit(-1);
}

int		create_server(int port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;
	int					ret;

	proto = getprotobyname("tcp");
	if (!proto)
		return (-1);
	sock = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	ret = bind(sock, (const struct sockaddr *)&sin, sizeof(sin));
	if (ret != 0)
	{
		perror("bind");
		return (-2);
	}
	ret = listen(sock, 42);
	if (ret != 0)
	{
		perror("listen");
		return (-3);
	}
	return (sock);
}

int		is_path_allowed(char *path)
{
	char	absolute_path[PATH_MAX];

	realpath(path, absolute_path);
	if (ft_strncmp(absolute_path, g_base_path, strlen(g_base_path)) != 0)
		return (0);
	return (1);
}

void	start_session(int cs)
{
	t_ft_p_request	request;
	int				r;

	if (fork() != 0)
		return ;
	while (42)
	{
		if ((r = read(cs, &request, sizeof(t_ft_p_request))) <= 0)
		{
			fprintf(stderr, "Connection closed by client\n");
			break ;
		}
		if (r != sizeof(t_ft_p_request))
		{
			fprintf(stderr, "Corrupted request received\n");
			break ;
		}
		execute_command(cs, request);
	}
	close(cs);
	exit(0);
}

int		main(int ac, char **av)
{
	int					port;
	int					sock;
	int					cs;
	socklen_t			cslen;
	struct sockaddr_in	csin;

	if (ac != 2)
		usage(av[0]);
	getcwd(g_base_path, PATH_MAX);
	port = atoi(av[1]);
	if ((sock = create_server(port)) < 0)
		return (sock);
	cslen = sizeof(struct sockaddr_in);
	while (42)
	{
		if ((cs = accept(sock, (struct sockaddr*)&csin, &cslen)) < 0)
		{
			perror("accept");
			return (-1);
		}
		fprintf(stderr, "Connection established\n");
		start_session(cs);
	}
	close(sock);
	return (0);
}
