/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 11:46:35 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 11:46:36 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

void	usage(char *name)
{
	fprintf(stderr, "Usage %s <host> <port>\n", name);
	exit(-1);
}

int		create_client(char *host, int port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;
	int					ret;

	proto = getprotobyname("tcp");
	if (!proto)
		return (-1);
	sock = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(host);
	ret = connect(sock, (const struct sockaddr *)&sin, sizeof(sin));
	if (ret != 0)
	{
		perror("connect");
		return (-2);
	}
	return (sock);
}

int		main(int ac, char **av)
{
	int		port;
	int		sock;
	char	*command;

	if (ac != 3)
		usage(av[0]);
	port = atoi(av[2]);
	sock = create_client(av[1], port);
	if (sock < 0)
		return (sock);
	while (42)
	{
		command = readline("ft_p > ");
		if (!command)
			break ;
		if (client_parse_command(sock, command) < 0)
			break ;
		free(command);
	}
	close(sock);
	return (0);
}
