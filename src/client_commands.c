/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_commands.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 12:57:37 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 12:57:39 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

static int	client_parse_command_3(int sock, char *command)
{
	if (ft_strlen(command) == 3 && ft_strncmp(command, "put", 3) == 0)
		fprintf(stderr, "Usage: put <path>\n");
	else if (ft_strncmp(command, "put ", 4) == 0)
	{
		if (ft_strlen(command) == 4)
		{
			fprintf(stderr, "Usage: put <path>\n");
			free(command);
			return (0);
		}
		if (send_request_put(sock, command + 4) < 0)
			return (-1);
	}
	else
		fprintf(stderr, "Unkown command: %s\n", command);
	return (0);
}

static int	client_parse_command_2(int sock, char *command)
{
	if (ft_strncmp(command, "cd ", 3) == 0)
	{
		if (ft_strlen(command) == 3)
		{
			fprintf(stderr, "Usage: cd <path>\n");
			return (0);
		}
		if (send_request_cd(sock, command + 3) < 0)
			return (-1);
	}
	else if (ft_strlen(command) == 3 && ft_strncmp(command, "get", 3) == 0)
		fprintf(stderr, "Usage: get <path>\n");
	else if (ft_strncmp(command, "get ", 4) == 0)
	{
		if (ft_strlen(command) == 4)
		{
			fprintf(stderr, "Usage: get <path>\n");
			return (0);
		}
		if (send_request_get(sock, command + 4) < 0)
			return (-1);
	}
	else
		return (client_parse_command_3(sock, command));
	return (0);
}

int			client_parse_command(int sock, char *command)
{
	if (ft_strcmp(command, "quit") == 0)
		return (-1);
	else if (ft_strlen(command) == 3 && ft_strncmp(command, "pwd", 3) == 0)
	{
		if (send_request_pwd(sock) < 0)
			return (-1);
	}
	else if (ft_strlen(command) == 2 && ft_strncmp(command, "ls", 2) == 0)
	{
		if (send_request_ls(sock) < 0)
			return (-1);
	}
	else if (ft_strlen(command) == 2 && ft_strncmp(command, "cd", 2) == 0)
		fprintf(stderr, "Usage: cd <path>\n");
	else
		return (client_parse_command_2(sock, command));
	return (0);
}
