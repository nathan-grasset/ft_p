/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 13:31:06 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 13:31:08 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

static t_ft_p_error	g_ftperrno = FT_P_EOK;

t_ft_p_error		get_error(void)
{
	return (g_ftperrno);
}

t_ft_p_error		set_error(t_ft_p_error err)
{
	g_ftperrno = err;
	return (err);
}

void				log_ft_p_error(t_ft_p_error err)
{
	if (err == FT_P_ENOTFOUND)
		fprintf(stderr, "ft_p error: File not found\n");
	if (err == FT_P_ETOOBIG)
		fprintf(stderr, "ft_p error: File exceeds size limit\n");
	if (err == FT_P_EBADDIR)
		fprintf(stderr, "ft_p error: No such directory\n");
	if (err == FT_P_EBADFILE)
		fprintf(stderr, "ft_p error: No such file\n");
	if (err == FT_P_EBODY_CORRUPTED)
		fprintf(stderr, "ft_p error: Body is corrupted\n");
	if (err == FT_P_ESERVER_INTERNAL)
		fprintf(stderr, "ft_p error: Internal server error\n");
	if (err == FT_P_EOUTPATH)
		fprintf(stderr, "ft_p error: Path must be inside base folder\n");
}

int					send_response_error(int socket, t_ft_p_error err)
{
	t_ft_p_response	r;

	r.error = err;
	r.content_size = 0;
	memset(r.message, 0, 256);
	strncpy(r.message, "FAILURE", 8);
	return (write(socket, &r, sizeof(t_ft_p_response)));
}
