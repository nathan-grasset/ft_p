/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_fs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ngrasset <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 13:24:41 by ngrasset          #+#    #+#             */
/*   Updated: 2018/06/13 13:24:42 by ngrasset         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

int		write_file_to_disk(char *filename, char *data, t_u32 size)
{
	int		fd;
	int		ret;

	fd = open(filename, O_CREAT | O_TRUNC | O_RDWR,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd < 0)
	{
		perror("open");
		return (set_error(FT_P_EBADFILE));
	}
	if ((ret = write(fd, data, size)) < 0)
		return (-1);
	close(fd);
	return (ret);
}

char	*read_file_secure(t_u32 size, int fd)
{
	char	*data;
	int		ret;

	if (size > MAX_FILE_SIZE)
	{
		fprintf(stderr, "Max file size exceeded\n");
		close(fd);
		set_error(FT_P_ETOOBIG);
		return (NULL);
	}
	data = (char *)malloc(size);
	ret = read(fd, data, size);
	if (ret != (t_s32)size)
	{
		fprintf(stderr, "Read error\n");
		free(data);
		close(fd);
		set_error(FT_P_ESERVER_INTERNAL);
		return (NULL);
	}
	close(fd);
	return (data);
}

char	*read_file_from_disk(char *filename, t_u32 *size_out)
{
	int				fd;
	struct stat		stats;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		perror("open");
		set_error(FT_P_EBADFILE);
		return (NULL);
	}
	if (fstat(fd, &stats) < 0)
	{
		perror("fstat");
		close(fd);
		set_error(FT_P_EBADFILE);
		return (NULL);
	}
	if (size_out)
		*size_out = stats.st_size;
	return (read_file_secure(stats.st_size, fd));
}

char	*get_file_name(char *path)
{
	char	*p;

	while (strlen(path) > 0 && path[strlen(path) - 1] == '/')
		path[strlen(path) - 1] = '\0';
	if (strlen(path) == 0)
		return (NULL);
	p = strrchr(path, '/');
	if (p == NULL)
		return (path);
	return (p + 1);
}
